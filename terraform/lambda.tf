# aws_lambda_function to control air conditioner
resource "aws_lambda_function" "ac_control_lambda" {
  function_name = "ac_control_lambda"
  role = aws_iam_role.lambda_role.arn
  filename = "files/empty_package.zip"
  handler = "ac_control_lambda.lambda_handler"
  runtime = "python3.8"
}

# aws_cloudwatch_event_rule scheduled action every minute
resource "aws_cloudwatch_event_rule" "event_rule" {
  name = "event_rule"
  schedule_expression = "rate(1 minute)"
}

# aws_cloudwatch_event_target to link the schedule event and the lambda function
resource "aws_cloudwatch_event_target" "event_target" {
  rule = aws_cloudwatch_event_rule.event_rule.name  
  target_id = "lambda"
  arn = aws_lambda_function.ac_control_lambda.arn  
  }


# aws_lambda_permission to allow CloudWatch (event) to call the lambda function
resource "aws_lambda_permission" "cloudwatch_permission" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ac_control_lambda.function_name
  principal = "events.amazonaws.com"
  source_arn = aws_cloudwatch_event_rule.event_rule.arn
}

